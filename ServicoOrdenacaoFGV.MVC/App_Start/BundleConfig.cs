﻿using System.Web;
using System.Web.Optimization;

namespace ServicoOrdenacaoFGV.MVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/AngularJs").Include(
                        "~/Scripts/AngularConfig/angular.js"));


            bundles.Add(new ScriptBundle("~/bundles/BookFiles").IncludeDirectory(
                        "~/Scripts/AngularModules/BookFiles", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/AngularModules").IncludeDirectory(
                        "~/Scripts/AngularModules/", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").IncludeDirectory(
                        "~/Content/", "*.css", true));
        }
    }
}
