﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoOrdenacaoFGV.MVC.Utils
{
    public static class RestSharpUtils
    {
        public static IRestResponse OpenRequest(string url, string controllerPath, Method method, object postObject = null)
        {
            var client = new RestClient(url);
            var request = new RestRequest(controllerPath, method);

            if (method == Method.POST)
            {
               
                var json = JsonConvert.SerializeObject(postObject);
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("Content-type", "application/json");
                request.AddBody(postObject);
            }

            return client.Execute(request);
        }
    }
}
