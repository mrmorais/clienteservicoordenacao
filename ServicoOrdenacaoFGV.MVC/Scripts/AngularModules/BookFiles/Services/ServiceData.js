﻿(function () {
    'use strict';

    var app = angular.module('BookApp');
    app.factory("ServiceData", ["$http", function ($http) {

        function GetBooks() {
            return $http.get("Book/GetAll", "GET");
        }
        function OrderBooks(param) {
            return $http.post("Book/OrderBooks", param, "POST");

        }
        return {
            GetBooks: GetBooks,
            OrderBooks: OrderBooks
        };

    }]);
})();