﻿(function () {
    'use strict';

    var app = angular.module('BookApp');
    app.controller("MainController", MainController);
    MainController.$inject = ["$scope", "ServiceData"];


    function MainController($scope, ServiceData) {
        var vm = this;
        vm.ordens = [];
        vm.ordensInterface = [];

        vm.attributes = { Title: "Titulo", AuthorName: "Autor", EditionYear: "Ano" };
        vm.orders = { Asc: "Crescente", Desc: "Decrescente" };
        ServiceData.GetBooks().success(function (data) {
            vm.books = data;
        });


        vm.remove = function (data) {
            vm.books.splice(data, 1);
        }

        vm.removeOrdem = function (data) {
            vm.ordensInterface.splice(data, 1);
            vm.ordens.splice(data, 1);
        }

        vm.insereLivro = function () {
            if (vm.authorName == undefined || vm.title == undefined) {
                alert("Por favor preencha ao menos o Autor e o Titulo do livro.");
                return;
            }

            var param = {
                AuthorName: vm.authorName,
                BookId: vm.books.length + 1,
                EditionYear: vm.editionYear,
                Title: vm.title
            }
            vm.books.push(param);
        }

        vm.insereOrdem = function () {
            var paramInterface = {};

            if (vm.atributoSelecionado == undefined || vm.ordemSelecionada == undefined) {
                alert("Você precisa selecionar as duas opções para poder inserir uma ordenação.");
                return;
            }

            if (vm.atributoSelecionado == "AuthorName")
                paramInterface.Attribute = "Autor"
            else if (vm.atributoSelecionado == "Title")
                paramInterface.Attribute = "Titulo"
            else
                paramInterface.Attribute = "Ano"

            if (vm.ordemSelecionada == "Asc")
                paramInterface.Order = "Crescente"
            else
                paramInterface.Order = "Decrescente"

            var param = { Attribute: vm.atributoSelecionado, Order: vm.ordemSelecionada }

            vm.ordensInterface.push(paramInterface);
            vm.ordens.push(param);

        }


        vm.enviaOrdenacao = function () {
            if (vm.ordens.length == 0) {
                alert("Por favor selecione pelo menos uma ordenação");
                return;
            }
            var Books = vm.books;
            var Ordenations = vm.ordens;
            var orderBooks = { Books: Books, Ordenations: Ordenations };

            ServiceData.OrderBooks(orderBooks).then(function (data) {
                vm.books = data.data;
            });

        }
    }
})();