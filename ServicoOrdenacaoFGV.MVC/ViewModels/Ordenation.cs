﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServicoOrdenacaoFGV.MVC.ViewModels
{
    public class Ordenation
    {
        public string Attribute { get; set; }
        public string Order { get; set; }
    }
}