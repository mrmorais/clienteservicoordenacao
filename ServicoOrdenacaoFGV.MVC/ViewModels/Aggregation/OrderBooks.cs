﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoOrdenacaoFGV.MVC.ViewModels.Aggregation
{
    public class OrderBooks
    {
        public List<Book> Books { get; set; }
        public List<Ordenation> Ordenations { get; set; }
    }
}
