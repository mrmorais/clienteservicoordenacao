﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoOrdenacaoFGV.MVC.ViewModels
{
    public partial class Book
    {
        public int BookId { get; set; }
        public string AuthorName { get; set; }
        public int EditionYear { get; set; }
        public string Title { get; set; }
    }
}
