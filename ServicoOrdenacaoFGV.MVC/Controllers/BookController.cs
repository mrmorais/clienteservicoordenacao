﻿using ServicoOrdenacaoFGV.MVC.Utils;
using System.Configuration;
using System.Web.Mvc;
using Newtonsoft.Json;
using RestSharp;
using ServicoOrdenacaoFGV.MVC.ViewModels;
using ServicoOrdenacaoFGV.MVC.ViewModels.Aggregation;

namespace ServicoOrdenacaoFGV.MVC.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public object GetAll()
        {
            return JsonConvert.DeserializeObject(RestSharpUtils.OpenRequest(ConfigurationManager.AppSettings["ApiUrl"], "Book/GetBooks", Method.GET).Content);
        }

        [HttpPost]
        public object OrderBooks(OrderBooks orderBooks)
        {
            return JsonConvert.DeserializeObject(RestSharpUtils.OpenRequest(ConfigurationManager.AppSettings["ApiUrl"], "Book/OrderBooks", Method.POST, orderBooks).Content);
        }
    }
}